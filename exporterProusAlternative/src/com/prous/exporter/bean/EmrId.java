package com.prous.exporter.bean;

import java.math.BigInteger;

public class EmrId {
	private BigInteger id;
	private BigInteger entryNumber;
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public BigInteger getEntryNumber() {
		return entryNumber;
	}
	public void setEntryNumber(BigInteger entryNumber) {
		this.entryNumber = entryNumber;
	}
}
